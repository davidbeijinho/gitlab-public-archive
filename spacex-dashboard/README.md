# SpaceX-dashboard


## Getting Started
Install dependencies
```bash
npm install
```

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## To use the SSG and SSR

```bash
npm run build
```
to build the pages and then you can run
```bash
npm run start
```
to start the productio server

## Testing
```bash
npm run test
```
Some basic basic testing was added using Jest with RTL, i have used the utilites provided by apollo to test the integration with GraphQL

### Tailwind 
Tailwind was used to help with the styling of the components

### Improvements
- I will split better the components, maybe create more helper components that can be used across the application
- I will add stuff like linter/ prettier, husk to run that jobs at the commit step, we want to bring feedback to the dev as fast as possible, even before the CI step
- I will add more testing
- Configuration from Enviroment variables

a lot of improvements
