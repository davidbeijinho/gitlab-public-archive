import { AppProps } from 'next/app'
import { ApolloProvider } from "@apollo/client";
import "tailwindcss/tailwind.css";
import { useApollo } from 'lib/apollo'

function App({ Component, pageProps }: AppProps) {
  const apolloClient = useApollo(pageProps)
  
  return <ApolloProvider client={apolloClient}>
    <Component {...pageProps} />
  </ApolloProvider>
}

export default App
