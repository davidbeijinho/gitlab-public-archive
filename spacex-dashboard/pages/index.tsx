import Head from 'next/head'
import { GetStaticProps } from 'next'
import { useQuery } from "@apollo/client";
import { initializeApollo, addApolloState } from 'lib/apollo'
import { LAUNCHES_PAST_QUERY } from 'queries/launchesPast.query'
import Card from 'components/Card/Card';
import Button from 'components/Button/Button';
import Heading from 'components/Heading/Heading';
import Container from 'components/Container/Container';
import SubHeading from 'components/SubHeading/Heading';

export default function Home() {
  const { data, loading } = useQuery(LAUNCHES_PAST_QUERY);
  if (loading) {
    return <div>Loading...</div>
  }
  return (
    <div className={"bg-gray-100"}>
      <Head>
        <title>SpaceX Dashboard</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Heading>Last Launches 🚀</Heading>
        {data.launchesPast.map(({ id, mission_name, details, launch_date_utc }) =>
          <Card
            header={<SubHeading>{mission_name}</SubHeading>}
            key={id}
            footer={<Button href={`/launches/${id}`} >{launch_date_utc}</Button>}
          >
            <p>{details ?? "No details avaliable"}</p>
          </Card>
        )}
      </Container>
    </div>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const apolloClient = initializeApollo()

  await apolloClient.query({
    query: LAUNCHES_PAST_QUERY,
  })

  return addApolloState(apolloClient, {
    props: {},
    revalidate: 1,
  })
}