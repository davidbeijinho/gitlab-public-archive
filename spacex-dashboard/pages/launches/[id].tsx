import { GetStaticPaths, GetStaticProps } from 'next'
import { LAUNCH_QUERY } from 'queries/launch.query'
import { LAUNCHES_PAST_QUERY } from 'queries/launchesPast.query'
import { initializeApollo } from 'lib/apollo'
import { useRouter } from 'next/router'
import Card from 'components/Card/Card'
import Button from 'components/Button/Button'
import Container from 'components/Container/Container';
import SubHeading from 'components/SubHeading/Heading'
import ImageContainer from 'components/ImageContainer/ImageContainer'
import PropTypes from 'prop-types';

const Launch = ({ launch }) => {
  const router = useRouter()
  if (router.isFallback) {
    return <div>Loading...</div>
  }
  const { links, details, id, mission_name } = launch;
  const url = links?.flickr_images?.[0] ?? "/not_found_image.jpg";
  const href = links?.wikipedia;
  const missionDetails = details ?? "No details avaliable";
  return (
    <Container>
      <Card header={<ImageContainer url={url} />}
        key={id}
        footer={href ? <Button href={href}>See More</Button> : "No link avaliable"}>
        <SubHeading>{mission_name}</SubHeading>
        <p>{missionDetails}</p>
      </Card>
    </Container>
  )
}
Launch.propTypes = {
  launch: PropTypes.shape({
    id: PropTypes.string,
    mission_name: PropTypes.string,
    details: PropTypes.string,
    launch_date_utc: PropTypes.string,
    links: PropTypes.shape({
      wikipedia: PropTypes.string,
      flickr_images: PropTypes.arrayOf(PropTypes.string),
    })
  }),
}

export default Launch

export const getStaticProps: GetStaticProps = async ({ params: { id } }) => {
  const apolloClient = initializeApollo()
  const { data: { launch } } = await apolloClient.query({
    query: LAUNCH_QUERY,
    variables: { id }
  })
  if (!launch) {
    return {
      notFound: true,
    }
  }
  return { props: { launch } }
}

export const getStaticPaths: GetStaticPaths = async () => {
  const apolloClient = initializeApollo()
  const { data } = await apolloClient.query({
    query: LAUNCHES_PAST_QUERY,
  })
  return {
    paths: data.launchesPast.map(({ id }) => ({
      params: { id }
    })),
    fallback: true,
  }
}