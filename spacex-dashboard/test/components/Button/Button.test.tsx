import React from "react";
import { render } from "@testing-library/react";
import { screen } from '@testing-library/dom'
import "@testing-library/jest-dom";

import Button from "components/Button/Button";

describe("Button", () => {
  it("should render the content passed as {children}", () => {
    render(<Button href="#">FAKE_CONTENT</Button>);

    const content = screen.getByText(
      /FAKE_CONTENT/i
    );

    expect(content).toBeInTheDocument();
  });
});
