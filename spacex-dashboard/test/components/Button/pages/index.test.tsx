import React from "react";
import { render, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MockedProvider } from "@apollo/client/testing";
import { LAUNCHES_PAST_QUERY } from 'queries/launchesPast.query'
import Home from "pages/index";
const mocks = [
    {
        request: {
            query: LAUNCHES_PAST_QUERY,
            variables: { limit: 10}
        },
        result: {
            data: {
                launchesPast: [
                    {
                        id: "MOCK_ID",
                        mission_name: "MOCK_MISSION_NAME",
                        launch_date_utc: "MOCK_LAUNCH_DATE_UTC",
                        details: "MOCK_DETAILS"
                    }
                ]
            }
        }
    }
]

describe("HomePage", () => {
    it("should render the loading step", () => {
        const { getByText } = render(<MockedProvider mocks={mocks}>
            <Home />
        </MockedProvider>);
        const content = getByText("Loading...");

        expect(content).toBeInTheDocument();
    });

    it("should render the content afetr loading", async () => {
        const { getByText } = render(<MockedProvider mocks={mocks}>
            <Home />
        </MockedProvider>);

        await waitFor(() => expect(getByText("Last Launches 🚀")).toBeInTheDocument())
    });


    it("should display the content of the mission", async () => {
        const { getByText } = render(<MockedProvider mocks={mocks}>
            <Home />
        </MockedProvider>);

        await waitFor(() => expect(getByText("MOCK_MISSION_NAME")).toBeInTheDocument())
        
        expect(getByText("MOCK_LAUNCH_DATE_UTC")).toBeInTheDocument()
        expect(getByText("MOCK_DETAILS")).toBeInTheDocument()
    });
});
