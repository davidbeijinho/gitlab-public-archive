import PropTypes from 'prop-types';

const Heading = ({ children }) => {
    return (
        <h1 className="font-bold text-3xl text-gray-900">{children}</h1>
    )
}

Heading.propTypes = {
    children: PropTypes.string.isRequired,
}

export default Heading;