import PropTypes from 'prop-types';

const Container = ({ children }) => {
    return (
        <main className="max-w-7xl px-6 mx-auto">
            {children}
        </main>
    )
}

Container.propTypes = {
    children: PropTypes.node.isRequired,
}

export default Container;