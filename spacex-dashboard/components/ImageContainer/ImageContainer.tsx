import PropTypes from 'prop-types';

const ImageContainer = ({ url }) => {
    return (
        <div className="h-96 bg-cover bg-center" style={{ backgroundImage: `url(${url})` }} />
    )
}

ImageContainer.propTypes = {
    url: PropTypes.string.isRequired,
}

export default ImageContainer;