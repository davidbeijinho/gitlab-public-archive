import PropTypes from 'prop-types';

const SubHeading = ({ children }) => {
    return (
        <h3 className="font-bold text-1xl text-gray-900">{children}</h3>
    )
}

SubHeading.propTypes = {
    children: PropTypes.string.isRequired,
}

export default SubHeading;