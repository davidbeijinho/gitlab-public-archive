import PropTypes from 'prop-types';
import Link from 'next/link'

const Button = ({ children, href }) => {
    return (
        <Link href={href} >
            <a className="bg-blue-500 shadow-md text-sm text-white font-bold py-3 md:px-8 px-4 hover:bg-blue-400 rounded uppercase">
                {children}
            </a>
        </Link>
    )
}

Button.propTypes = {
    children: PropTypes.node.isRequired,
    href: PropTypes.string.isRequired,
}

export default Button;