import PropTypes from 'prop-types';

const Card = ({ children, header, footer }) => {
    return (
        <div className="py-8">
            <div className="overflow-hidden shadow-md">
                <div className="px-6 py-4 bg-white border-b border-gray-200 font-bold uppercase">
                    {header}
                </div>
                <div className="p-6 bg-white border-b border-gray-200">
                    {children}
                </div>
                <div className="p-6 bg-white border-gray-200 text-right">
                    {footer}
                </div>
            </div>
        </div>
    )
}

Card.propTypes = {
    children: PropTypes.node,
    header: PropTypes.node,
    footer: PropTypes.node,
}

export default Card;