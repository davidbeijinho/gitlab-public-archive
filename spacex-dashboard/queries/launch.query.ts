import { gql } from "@apollo/client";

export const LAUNCH_QUERY = gql`
query launch($id: ID!) {
    launch(id: $id) {
      id
      mission_name
      details
      launch_date_utc
      links {
        wikipedia
        flickr_images
      }
    }
  }
`;
