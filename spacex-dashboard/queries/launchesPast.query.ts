import { gql } from "@apollo/client";

export const LAUNCHES_PAST_QUERY = gql`
  query launchesPast($limit: Int = 10) {
    launchesPast(limit: $limit) {
      id
      mission_name
      details
      launch_date_utc
    }
  }
`;
