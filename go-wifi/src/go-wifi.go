package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/theojulienne/go-wireless"
)

func main() {

	http.HandleFunc("/", HelloHandler)
	http.HandleFunc("/connections", ConnectionsHandler)
	http.HandleFunc("/interfaces", InterfacesHandler)

	fmt.Println("Server started at port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func HelloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, there v0.0.3 \n")
}

func InterfacesHandler(w http.ResponseWriter, r *http.Request) {
	ifaces := wireless.Interfaces()
	fmt.Println(ifaces)
	fmt.Fprintf(w, "Interfaces \n")
	fmt.Fprint(w, ifaces)
}

func ConnectionsHandler(w http.ResponseWriter, r *http.Request) {

	wc, err := wireless.NewClient("wlan1")
	if err != nil {
		log.Fatal(err)
	}
	defer wc.Close()

	aps, err := wc.Scan()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(aps)
	// fmt.Fprint(w, aps)
	fmt.Fprintf(w, "Connections \n")

	// // scans, err := wpacfg.ScanNetworks()
	// // if err != nil {
	// // 	log.Fatal(err)
	// // }
	// // fmt.Println(scans)

	// wifis, err := wifiscan.Scan()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// for _, wifi := range wifis {
	// 	fmt.Print(wifi)
	// 	fmt.Println(wifi.SSID, wifi.RSSI)
	// 	fmt.Fprintf(w, wifi.SSID)
	// 	// fmt.Fprintf(w, string(rune(wifi.RSSI)))

	// }

	// stations, err := network.DetectAvailableStations()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println(stations)
	// // if len(stations) != 0 {
	// // 	fmt.Println(stations[0])
	// // } else {
	// // 	fmt.Println("no stations")
	// // }

	// connectedInterfaces, err := network.GetConnectedWifiInterfaces()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// fmt.Println(connectedInterfaces[0])

}

func panicError(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func fatalError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
