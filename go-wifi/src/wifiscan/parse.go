package wifiscan

import (
	"bufio"
	"strconv"
	"strings"
)

// Wifi is the data structure containing the basic
// elements
type Wifi struct {
	SSID  string `json:"ssid"`
	RSSI  int    `json:"rssi"`
	ESSID string `json:"essid"`
}

func Parse(output string) (wifis []Wifi, err error) {
	scanner := bufio.NewScanner(strings.NewReader(output))
	w := Wifi{}
	wifis = []Wifi{}
	for scanner.Scan() {
		line := scanner.Text()
		// if w.SSID == "" {
		if strings.Contains(line, "Address") {
			fs := strings.Fields(line)
			if len(fs) == 5 {
				w.SSID = strings.ToLower(fs[4])
			}
		}
		if strings.Contains(line, "ESSID") {
			// fs := strings.Fields(line)
			var splits = strings.Split(line, "ESSID:")
			// if len(fs) == 5 {
			w.ESSID = strings.ToLower(splits[1])
			// }
		}

		// else {
		// 	continue
		// }
		// } else {
		if strings.Contains(line, "Signal level=") {
			level, errParse := strconv.Atoi(strings.Split(strings.Split(strings.Split(line, "level=")[1], "/")[0], " dB")[0])
			if errParse != nil {
				continue
			}
			if level > 0 {
				level = (level / 2) - 100
			}
			w.RSSI = level
		}
		// }
		if w.SSID != "" && w.RSSI != 0 {
			wifis = append(wifis, w)
			w = Wifi{}
		}
	}
	return
}
