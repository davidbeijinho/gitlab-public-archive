module go-wifi

go 1.15

require (
	github.com/henrikkorsgaard/wifi v0.0.0-20190515070100-0106fb4648e9
	github.com/mdlayher/netlink v1.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/schollz/logger v1.2.0
	github.com/theojulienne/go-wireless v1.0.0
)
