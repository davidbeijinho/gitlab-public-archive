## restart with: 
systemctl restart go_wifi

## stop with: 
systemctl stop go_wifi

## disable with: 
systemctl disable go_wifi


# Enable interface

ip link set wlan1 up