#include <Arduino.h>

#include <project_config.h>
#include <wifi.h>
#include <influx.h>

#include <dht11.h>
#include <humidity.h>
// #include <light.h>

wifi myWifi;
influx myInflux;

dht11 myDht11;
humidity myHumidity;
// light mylight;

void setup()
{
  Serial.begin(9600);

  myWifi.setup();

  myDht11.setup();
  myHumidity.setup();
  // mylight.setup();
}

void loop()
{
  myDht11.printData(myInflux);
  myHumidity.printData(myInflux);
  // mylight.printData(myInflux);
  delay(1000 * 60);
    // delay(1000 );
}
