#include <InfluxDbClient.h>

#include <project_config.h>

#include "influx.h"

InfluxDBClient client(INFLUXDB_URL, INFLUXDB_DB_NAME);

influx::influx()
{
}

void influx::writeData(Point sensor)
{
    if (!client.writePoint(sensor))
    {
        Serial.print("InfluxDB write failed: ");
        Serial.println(client.getLastErrorMessage());
    }
    sensor.clearFields();
}