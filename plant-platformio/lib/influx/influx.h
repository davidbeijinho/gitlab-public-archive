#ifndef influx_H
#define influx_H

#include <InfluxDbClient.h>

class influx
{
public:
  influx();
  void writeData(Point sensor);
};

#endif //  influx_H