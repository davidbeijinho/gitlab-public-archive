#ifndef light_H
#define light_H

#include <influx.h>

class light
{
public:
  light();
  void setup();
  void printData(influx myInflux);
};

#endif //  light_H