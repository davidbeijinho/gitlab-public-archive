#include "light.h"
#include <Arduino.h>
#include <InfluxDbClient.h>

#include <project_config.h>

#include <influx.h>

Point sensor("light");

light::light()
{
}

void light::setup()
{
    sensor.addTag("device", DEVICE);
    // sensor.addTag("SSID", WiFi.SSID());
}

void light::printData(influx myInflux)
{
    int analogValue = analogRead(A0);
    Serial.print("ANALOG: ");
    Serial.println(analogValue);

    sensor.addField("analog", analogValue);

    myInflux.writeData(sensor);
}