#ifndef dht11_H
#define dht11_H

#include <influx.h>

class dht11
{
public:
  dht11();
  void setup();
  void printData(influx myInflux);
};

#endif //  dht11_H