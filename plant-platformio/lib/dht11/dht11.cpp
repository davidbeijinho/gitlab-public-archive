#include "dht11.h"
#include "DHT.h"

#include <project_config.h>

#include <influx.h>

#define DHTPIN D1 // Digital pin connected to the DHT dht11Sensor

#define DHTTYPE DHT11 // DHT 11

DHT dht(DHTPIN, DHTTYPE);

Point dht11Sensor("dht11");

dht11::dht11()
{
}

void dht11::setup()
{
    dht.begin();
    dht11Sensor.addTag("device", DEVICE);
}

void dht11::printData(influx myInflux)
{
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    float h = dht.readHumidity();
    // Read temperature as Celsius (the default)
    float t = dht.readTemperature();

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t))
    {
        Serial.println(F("Failed to read from DHT sensor!"));
        return;
    }

    // Compute heat index in Celsius (isFahreheit = false)
    float hic = dht.computeHeatIndex(t, h, false);

    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%"));
    dht11Sensor.addField("humidity", h);

    Serial.print(F("Temperature: "));
    Serial.print(t);
    Serial.print(F("°C"));
    dht11Sensor.addField("temperature", t);

    Serial.print(F("Heat index: "));
    Serial.print(hic);
    Serial.print(F("°C "));
    dht11Sensor.addField("Heat index", hic);

    myInflux.writeData(dht11Sensor);
}