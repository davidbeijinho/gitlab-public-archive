#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <project_config.h>

#include "wifi.h"
wifi::wifi()
{
}

void wifi::setup()
{
    Serial.print("Connecting to ");
    Serial.println(WIFI_SSID);

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}