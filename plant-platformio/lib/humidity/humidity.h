#ifndef humidity_H
#define humidity_H

#include <influx.h>

class humidity
{
public:
  humidity();
  void setup();
  void printData(influx myInflux);
};

#endif //  humidity_H