#include "humidity.h"
#include <Arduino.h>
#include <InfluxDbClient.h>

#include <project_config.h>

#include <influx.h>

Point sensor("humidity");

humidity::humidity()
{
}

void humidity::setup()
{
    sensor.addTag("device", DEVICE);
    // sensor.addTag("SSID", WiFi.SSID());
}

void humidity::printData(influx myInflux)
{
    int analogValue = analogRead(A0);
    Serial.print("ANALOG: ");
    Serial.println(analogValue);

    int digitalValue = digitalRead(D0);
    Serial.print("DIGITAL: ");
    Serial.println(digitalValue);

    sensor.addField("analog", analogValue);
    sensor.addField("digital", digitalValue);

    myInflux.writeData(sensor);
}