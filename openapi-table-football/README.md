# Table Football APP

tldr;
to start all the parts of the application
```
cp .env.sample .env
```
```
docker-compose up
```


## API containing the API Layer
Application created using [NestJs](https://nestjs.com/)

to start the application you need to:
### install dependencies
```
npm install
```

### start the application
```
npm run start
```
it needs *db*  layer

### Open api documentation

```
http://localhost:9000/api/
```

## WEB containg the frontend web layer

Application created using [CRA](https://create-react-app.dev/)

to start the application you need to:

### install dependencies
```
npm install
```

### start the application
```
npm run start
```

### Web application
```
http://localhost:3000
```

### star using docker
```
docker-compose up web
```
it needs *db* and *api* layer

## DB to save the data using postgres
### star using docker
```
docker-compose up db
```

adminer application is provided

```
docker-compose up adminer
```

access the adminer web interface
```
http://localhost:8080
```


## API Client 

frontend code to interact with the api generated from the specs using [openApi](https://openapi-generator.tech/)

used by the web layer
```
./web/src/client
```

----

Notes:
A lot of functionality is missing, time was a key factor, and try to make everything work plus creating a dockerfiles and docker-compose to connect everthing

If this was a real project some of the next steps
would be:

- fix prolems with .env configuration, all the config should come from enviroment variables
- add some testing for all layers
- create a better way to update the generation of specs(include in CI pipeline)
- create a better process to update the client part (include in CI pipeline), publiush to npm and install as dependencie in web layer
- On the frontend part a lot of styling, smaller components and reusable, maybe create a separeted repo just for that
- for the API create more examples for the openapi documentation
- create more endpoint as needed for the frontend application