npx @openapitools/openapi-generator-cli generate \
-i specs.json \
-g javascript \
-o ./web/src/client \
--package-name table_football_client \
-c openapitools.json