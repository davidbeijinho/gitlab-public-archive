{
  "openapi": "3.0.0",
  "info": {
    "title": "Table Football API",
    "description": "My table-football api",
    "version": "1.0.0",
    "contact": {}
  },
  "tags": [
    {
      "name": "Football",
      "description": ""
    }
  ],
  "servers": [
    {
      "url": "http://localhost:9000",
      "description": "local dev server"
    }
  ],
  "components": {
    "schemas": {
      "CreatePlayerDto": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "description": "The Name of the Player",
            "example": "David"
          },
          "teamId": {
            "type": "number",
            "description": "The Id of the Team that the player belongs",
            "example": 1
          }
        },
        "required": [
          "name",
          "teamId"
        ]
      },
      "Team": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "example": "The magicians",
            "description": "The name of the Team"
          },
          "id": {
            "type": "number"
          }
        },
        "required": [
          "name",
          "id"
        ]
      },
      "Player": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "example": "David",
            "description": "The name of the Player"
          },
          "team": {
            "example": 1,
            "description": "The Id of the Team that the player belongs",
            "allOf": [
              {
                "$ref": "#/components/schemas/Team"
              }
            ]
          },
          "id": {
            "type": "number"
          }
        },
        "required": [
          "name",
          "team",
          "id"
        ]
      },
      "CreateTeamDto": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "description": "The Name of the Team",
            "example": "The magicians"
          }
        },
        "required": [
          "name"
        ]
      },
      "CreateGameDto": {
        "type": "object",
        "properties": {
          "homePlayerId": {
            "type": "number",
            "description": "The Player Id of the home Player",
            "example": 76
          },
          "visitorPlayerId": {
            "type": "number"
          },
          "homeResult": {
            "type": "number",
            "description": "The number of goals of the home Player",
            "example": 4,
            "minimum": 0
          },
          "visitorResult": {
            "type": "number",
            "description": "The number of goals of the visitor Player",
            "example": 2,
            "minimum": 0
          },
          "finished": {
            "type": "boolean",
            "description": "Flag that indicates if the game was finished",
            "example": false
          }
        },
        "required": [
          "homePlayerId",
          "visitorPlayerId",
          "homeResult",
          "visitorResult",
          "finished"
        ]
      },
      "Game": {
        "type": "object",
        "properties": {
          "homePlayer": {
            "example": 123,
            "description": "The Player Id of the home Player",
            "allOf": [
              {
                "$ref": "#/components/schemas/Player"
              }
            ]
          },
          "visitorPlayer": {
            "example": 412,
            "description": "The Player Id of the visitor Player",
            "allOf": [
              {
                "$ref": "#/components/schemas/Player"
              }
            ]
          },
          "homeResult": {
            "type": "number",
            "example": 12,
            "description": "The number of goals for the home Player"
          },
          "visitorResult": {
            "type": "number",
            "example": 24,
            "description": "The number of goals for the visitor Player"
          },
          "finished": {
            "type": "boolean",
            "example": false,
            "description": "flag that indicates if the game as finished"
          },
          "id": {
            "type": "number"
          }
        },
        "required": [
          "homePlayer",
          "visitorPlayer",
          "homeResult",
          "visitorResult",
          "finished",
          "id"
        ]
      }
    }
  },
  "paths": {
    "/": {
      "get": {
        "operationId": "AppController_getHello",
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/players": {
      "get": {
        "operationId": "PlayersController_getAll",
        "summary": "Get all players",
        "parameters": [],
        "responses": {
          "200": {
            "description": "The players records"
          }
        },
        "tags": [
          "players"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      },
      "post": {
        "operationId": "PlayersController_create",
        "summary": "Create player",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreatePlayerDto"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "The record has been successfully created.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Player"
                }
              }
            }
          },
          "400": {
            "description": "Forbidden."
          }
        },
        "tags": [
          "players"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      }
    },
    "/players/{id}": {
      "get": {
        "operationId": "PlayersController_findOne",
        "summary": "Get player by {id}",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The found record",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Player"
                }
              }
            }
          },
          "404": {
            "description": "Player not found"
          }
        },
        "tags": [
          "players"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      }
    },
    "/teams": {
      "get": {
        "operationId": "TeamsController_getAll",
        "summary": "Get all teams",
        "parameters": [],
        "responses": {
          "200": {
            "description": "The team records"
          }
        },
        "tags": [
          "teams"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      },
      "post": {
        "operationId": "TeamsController_create",
        "summary": "Create team",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreateTeamDto"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "The record has been successfully created.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Team"
                }
              }
            }
          }
        },
        "tags": [
          "teams"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      }
    },
    "/teams/{id}": {
      "get": {
        "operationId": "TeamsController_findOne",
        "summary": "Get team by {id}",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The found record",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Team"
                }
              }
            }
          },
          "404": {
            "description": "Team not found"
          }
        },
        "tags": [
          "teams"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      }
    },
    "/games": {
      "get": {
        "operationId": "GamesController_getAll",
        "summary": "Get all games",
        "parameters": [],
        "responses": {
          "200": {
            "description": "The games records"
          }
        },
        "tags": [
          "games"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      },
      "post": {
        "operationId": "GamesController_create",
        "summary": "Create game",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreateGameDto"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "The record has been successfully created.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Game"
                }
              }
            }
          },
          "400": {
            "description": "Forbidden."
          }
        },
        "tags": [
          "games"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      }
    },
    "/games/{id}": {
      "get": {
        "operationId": "GamesController_findOne",
        "summary": "Get game by {id}",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The found record",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Game"
                }
              }
            }
          },
          "404": {
            "description": "Game not found"
          }
        },
        "tags": [
          "games"
        ],
        "security": [
          {
            "bearer": []
          }
        ]
      }
    }
  }
}