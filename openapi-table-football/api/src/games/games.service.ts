import { Injectable } from '@nestjs/common';
import { CreateGameDto } from './dto/create-game.dto';
import { Game } from './entities/game.entity';
import { PlayersService } from '../players/players.service';
import { ForbiddenException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class GamesService {
  constructor(
    @InjectRepository(Game)
    private gamesRepository: Repository<Game>,
    private readonly playersService: PlayersService,
  ) { }

  getAll(): Promise<Game[]> {
    return this.gamesRepository.find();
  }

  async create(game: CreateGameDto): Promise<Game> {
    const homePlayer = await this.playersService.findOne(game.homePlayerId);
    if (game.homePlayerId === game.visitorPlayerId) {
      throw new ForbiddenException(
        `Players have to be different`,
      );
    } else if (homePlayer) {
      const visitorPlayer = await this.playersService.findOne(
        game.visitorPlayerId,
      );
      if (visitorPlayer) {
        return this.gamesRepository.save({
          ...game,
          visitorPlayer,
          homePlayer,
        });
      } else {
        throw new ForbiddenException(
          `Player with Id ${game.homePlayerId} was not found`,
        );
      }
    } else {
      throw new ForbiddenException(
        `Player with Id ${game.visitorPlayerId} was not found`,
      );
    }
  }

  findOne(id: number): Promise<Game> {
    return this.gamesRepository.findOne({ id });
  }
}
