import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Player } from '../../players/entities/player.entity';

@Entity()
export class Game {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    example: 123,
    description: 'The Player Id of the home Player',
  })
  @ManyToOne(() => Player, (player) => player.id, {
    eager: true
  })
  homePlayer: Player;

  @ApiProperty({
    example: 412,
    description: 'The Player Id of the visitor Player',
  })
  @ManyToOne(() => Player, (player) => player.id, {
    eager: true
  })
  visitorPlayer: Player;

  @ApiProperty({
    example: 12,
    description: 'The number of goals for the home Player',
  })
  @Column()
  homeResult: number;

  @ApiProperty({
    example: 24,
    description: 'The number of goals for the visitor Player',
  })
  @Column()
  visitorResult: number;

  @ApiProperty({
    example: false,
    description: 'flag that indicates if the game as finished',
  })
  @Column()
  finished: boolean;
}
