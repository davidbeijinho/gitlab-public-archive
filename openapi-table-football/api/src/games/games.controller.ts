import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiBody,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { GamesService } from './games.service';
import { CreateGameDto } from './dto/create-game.dto';
import { Game } from './entities/game.entity';
import { NotFoundException } from '@nestjs/common';

@ApiBearerAuth()
@ApiTags('games')
@Controller('games')
export class GamesController {
  constructor(private readonly gamesService: GamesService) {}
  @Get()
  @ApiOperation({ summary: 'Get all games' })
  @ApiResponse({ status: 200, description: 'The games records' })
  async getAll(): Promise<Game[]> {
    return this.gamesService.getAll();
  }

  @Post()
  @ApiBody({ type: CreateGameDto })
  @ApiOperation({ summary: 'Create game' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: Game,
  })
  @ApiBadRequestResponse({ description: 'Forbidden.' })
  async create(@Body() createGameDto: CreateGameDto): Promise<Game> {
    return this.gamesService.create(createGameDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get game by {id}' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Game,
  })
  @ApiNotFoundResponse({ description: 'Game not found' })
  async findOne(@Param('id') id: string): Promise<Game> {
    const game = await this.gamesService.findOne(+id);
    if (game) {
      return game;
    }
    throw new NotFoundException();
  }
}
