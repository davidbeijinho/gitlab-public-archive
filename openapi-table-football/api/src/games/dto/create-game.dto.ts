import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsBoolean } from 'class-validator';

export class CreateGameDto {
  @ApiProperty({
    description: 'The Player Id of the home Player',
    type: Number,
    example: 76,
  })
  @IsNumber()
  readonly homePlayerId: number;

  @ApiProperty()
  @IsNumber()
  readonly visitorPlayerId: number;

  @ApiProperty({
    description: 'The number of goals of the home Player',
    type: Number,
    example: 4,
    minimum: 0,
  })
  @IsNumber()
  readonly homeResult: number;

  @ApiProperty({
    description: 'The number of goals of the visitor Player',
    type: Number,
    example: 2,
    minimum: 0,
  })
  @IsNumber()
  readonly visitorResult: number;

  @ApiProperty({
    description: 'Flag that indicates if the game was finished',
    type: Boolean,
    example: false,
  })
  @IsBoolean()
  readonly finished: boolean;
}
