import { writeFileSync } from 'fs';

export const updateSpecsFile = (data) => {
    writeFileSync("../specs.json", JSON.stringify(data, null, 2)); 
}