import { Injectable } from '@nestjs/common';
import { CreatePlayerDto } from './dto/create-player.dto';
import { Player } from './entities/player.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TeamsService } from '../teams/teams.service';
import { ForbiddenException } from '@nestjs/common';

@Injectable()
export class PlayersService {
  constructor(
    @InjectRepository(Player)
    private playersRepository: Repository<Player>,
    private readonly teamsService: TeamsService,
  ) {}

  getAll(): Promise<Player[]> {
    return this.playersRepository.find();
  }

  async create(player: CreatePlayerDto): Promise<Player> {
    const team = await this.teamsService.findOne(player.teamId);
    if (team) {
      return this.playersRepository.save({ ...player, team });
    } else {
      throw new ForbiddenException(`Team ${player.teamId} was not found`);
    }
  }

  findOne(id: number): Promise<Player> {
    return this.playersRepository.findOne({ id });
  }
}
