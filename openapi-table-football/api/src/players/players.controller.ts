import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { PlayersService } from './players.service';
import { CreatePlayerDto } from './dto/create-player.dto';
import { Player } from './entities/player.entity';
import { NotFoundException } from '@nestjs/common';

@ApiBearerAuth()
@ApiTags('players')
@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) { }

  @Get()
  @ApiOperation({ summary: 'Get all players' })
  @ApiResponse({ status: 200, description: 'The players records' })
  async getAll(): Promise<Player[]> {
    return this.playersService.getAll();
  }

  @Post()
  @ApiOperation({ summary: 'Create player' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: Player,
  })
  @ApiBadRequestResponse({ description: 'Forbidden.' })
  async create(@Body() createPlayerDto: CreatePlayerDto): Promise<Player> {
    return this.playersService.create(createPlayerDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get player by {id}' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Player,
  })
  @ApiNotFoundResponse({ description: 'Player not found' })
  async findOne(@Param('id') id: string): Promise<Player> {
    const player = await this.playersService.findOne(+id);
    if (player) {
      return player;
    }
    throw new NotFoundException();
  }
}
