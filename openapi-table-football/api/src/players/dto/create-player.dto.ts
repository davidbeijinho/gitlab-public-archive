import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class CreatePlayerDto {
  @ApiProperty({
    description: 'The Name of the Player',
    type: String,
    example: 'David',
  })
  @IsString()
  readonly name: string;

  @ApiProperty({
    description: 'The Id of the Team that the player belongs',
    type: Number,
    example: 1,
  })
  @IsNumber()
  readonly teamId: number;
}
