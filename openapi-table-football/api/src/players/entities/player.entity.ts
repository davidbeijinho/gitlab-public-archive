import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne ,JoinColumn} from 'typeorm';
import { Team } from '../../teams/entities/team.entity';

@Entity()
export class Player {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'David', description: 'The name of the Player' })
  @Column()
  name: string;

  @ApiProperty({
    example: 1,
    description: 'The Id of the Team that the player belongs',
  })
  @ManyToOne(() => Team, (team) => team.id, {
    eager: true
})
  team: Team;
}
