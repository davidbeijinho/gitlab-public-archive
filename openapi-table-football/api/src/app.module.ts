import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PlayersModule } from './players/players.module';
import { TeamsModule } from './teams/teams.module';
import { GamesModule } from './games/games.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from './teams/entities/team.entity';
import { ConfigModule } from '@nestjs/config';
import { Player } from './players/entities/player.entity';
import { Game } from './games/entities/game.entity';

const DB_CONFIG =
  process.env.NODE_ENV !== 'production'
    ? {
        database: 'postgres',
        host: 'db',
        port: 5432,
        username: 'root',
        password: 'root',
      }
    : {
        database: process.env.POSTGRES_DB,
        host: process.env.POSTGRES_HOST,
        port: parseInt(process.env.POSTGRES_PORT, 10),
        username: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
      };

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      ...DB_CONFIG,
      synchronize: true, // would not use in production, ok for testing
      type: 'postgres',
      entities: [Team, Player, Game],
      autoLoadEntities: true,
    }),
    PlayersModule,
    TeamsModule,
    GamesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
