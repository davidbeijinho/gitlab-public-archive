import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { updateSpecsFile } from './openApi';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors(); // during development
  const config = new DocumentBuilder()
    .setTitle('Table Football API')
    .setDescription('My table-football api')
    .setVersion('1.0.0')
    .addTag('Football')
    .addServer("http://localhost:9000", "local dev server")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  updateSpecsFile(document);
  SwaggerModule.setup('api', app, document);
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  await app.listen(9000);
}
bootstrap();
