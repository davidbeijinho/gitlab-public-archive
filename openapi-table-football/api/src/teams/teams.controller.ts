import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  NotFoundException,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiCreatedResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { TeamsService } from './teams.service';
import { CreateTeamDto } from './dto/create-team.dto';
import { Team } from './entities/team.entity';

@ApiBearerAuth()
@ApiTags('teams')
@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all teams' })
  @ApiResponse({ status: 200, description: 'The team records' })
  async getAll(): Promise<Team[]> {
    return this.teamsService.getAll();
  }

  @Post()
  @ApiOperation({ summary: 'Create team' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.',
    type: Team,
  })
  async create(@Body() createTeamDto: CreateTeamDto): Promise<Team> {
    return this.teamsService.create(createTeamDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get team by {id}' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Team,
  })
  @ApiNotFoundResponse({ description: 'Team not found' })
  async findOne(@Param('id') id: string): Promise<Team> {
    const team = await this.teamsService.findOne(+id);
    if (team) {
      return team;
    }
    throw new NotFoundException();
  }
}
