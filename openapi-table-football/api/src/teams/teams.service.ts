import { Injectable } from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { Team } from './entities/team.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TeamsService {
  constructor(
    @InjectRepository(Team)
    private usersRepository: Repository<Team>,
  ) {}

  getAll(): Promise<Team[]> {
    return this.usersRepository.find();
  }

  create(team: CreateTeamDto): Promise<Team> {
    return this.usersRepository.save(team);
  }

  findOne(id: number): Promise<Team> {
    return this.usersRepository.findOne({ id });
  }
}
