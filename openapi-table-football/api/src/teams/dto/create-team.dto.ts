import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateTeamDto {
  @ApiProperty({
    description: 'The Name of the Team',
    type: String,
    example: 'The magicians',
  })
  @IsString()
  readonly name: string;
}
