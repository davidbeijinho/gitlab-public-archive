import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Team {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    example: 'The magicians',
    description: 'The name of the Team',
  })
  @Column()
  name: string;
}
