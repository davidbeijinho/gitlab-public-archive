import React from "react";

const ListPlayers = ({ players }) => {
  if (players.length === 0) {
    return <h3>No Players found</h3>
  }
  return <div>
    <h3>Players list</h3>
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Team</th>
        </tr>
      </thead>
      <tbody>
        {players.map(({ name, id, team: { name: teamName } }) => <tr key={id}>
          <td>{id}</td>
          <td>{name}</td>
          <td>{teamName}</td>
        </tr>)}
      </tbody>
    </table>
  </div>
}

export default ListPlayers;