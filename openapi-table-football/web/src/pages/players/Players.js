import React, { useEffect, useState } from "react"
import AddPlayer from 'pages/players/AddPlayer';
import ListPlayers from 'pages/players/ListPlayers';
import api from 'lib/api';

const Players = () => {
    const [addMode, setAddMode] = useState(false);
    const [players, setPlayers] = useState([]);
    const loadPlayers = () => {
        api.playersApi.playersControllerGetAll((error, data, { body }) => {
            if (error) {
                console.error(error);
            } else {
                setPlayers(body);
            }
        });
    }
    useEffect(() => {
        loadPlayers()
    }, [])
    const onAdd = (dto) => {
        api.playersApi.playersControllerCreate(dto, (error) => {
            if (error) {
                console.error(error);
            } else {
                setAddMode(false);
                loadPlayers();
            }
        });
    }
    return <>
        <h1>Players page</h1>
        <button onClick={() => setAddMode(!addMode)}>{addMode ? "Show players list" : "Add new player"}</button>
        { addMode ? <AddPlayer onSubmit={onAdd} /> : <ListPlayers players={players} />}
    </>
}

export default Players