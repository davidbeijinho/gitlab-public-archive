import React, { useEffect, useState } from "react";
import api from 'lib/api';

const AddPlayer = ({ onSubmit }) => {
    const [teams, setTeams] = useState([]);
    const [name, setName] = useState();
    const [teamId, setTeamId] = useState();
    const handleSubmit = async () => {
        const createPlayerDto = new api.TableFootball.CreatePlayerDto(name, Number(teamId));
        onSubmit(createPlayerDto);
    }
    useEffect(() => {
        api.teamsApi.teamsControllerGetAll((error, data, { body }) => {
            if (error) {
                console.error(error);
            } else {
                setTeams(body);
                setTeamId(body?.[0].id)
            }
        });
    }, [])
    if (teams.length === 0) {
        return <h3>No teams found, cant add player</h3>
    }
    return <div>
        <h3>Add new Player</h3>
        <label>Name</label >
        <input onChange={({ target }) => { setName(target.value) }} required />
        <label>Team</label >
        <select name="teams" id="teams" required onChange={({ target }) => { setTeamId(target.value) }} >
            {
                teams.map((team) => <option key={team.id} value={team.id} >{team.name}</option>)
            }
        </select>
        <br />
        <button onClick={handleSubmit}>Add</button>
    </div>
}

export default AddPlayer;