import React, { useEffect, useState } from "react";
import AddTeam from 'pages/teams/AddTeam';
import ListTeams from 'pages/teams/ListTeams';
import api from 'lib/api';

const Teams = () => {
    const [addMode, setAddMode] = useState(false);
    const [teams, setTeams] = useState([]);
    const loadTeams = () => {
        api.teamsApi.teamsControllerGetAll((error, data, {body}) => {
            if (error) {
                console.error(error);
            } else {
                setTeams(body);
            }
        });
    }
    useEffect(loadTeams, [])
    const onAdd = (dto) => {
        api.teamsApi.teamsControllerCreate(dto, (error) => {
            if (error) {
                console.error(error);
            } else {
                setAddMode(false);
                loadTeams();
            }
        });
    }
    return <>
        <h1>Teams page</h1>
        <button onClick={() => setAddMode(!addMode)}>{addMode ? "Show team list" : "Add new Team"}</button>
        { addMode ? <AddTeam onSubmit={onAdd} /> : teams && <ListTeams teams={teams} />}
    </>
}

export default Teams;