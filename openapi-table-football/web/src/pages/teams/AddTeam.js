import React, { useState } from "react";
import api from 'lib/api';

const AddTeam = ({ onSubmit }) => {
    const [name, setName] = useState("");
    const handleSubmit = async () => {
        const createTeamDto = new api.TableFootball.CreateTeamDto(name);
        onSubmit(createTeamDto);
    }

    return <div>
        <h3>Add new Team</h3>
        <label >Name</label>
        <input onChange={({ target }) => { setName(target.value) }} required />
        <br />
        <button onClick={handleSubmit}>Add</button>
    </div>
}

export default AddTeam;