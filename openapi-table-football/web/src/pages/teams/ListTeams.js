import React from "react";

const ListTeams = ({ teams }) => {
  if (teams.length === 0) {
    return <h3>No teams found</h3>
  }
  return <div>
    <h3>Teams list</h3>
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {teams.map(({ name, id }) => <tr key={id}>
          <td>{id}</td>
          <td>{name}</td>
        </tr>)}
      </tbody>
    </table>
  </div>
}

export default ListTeams;