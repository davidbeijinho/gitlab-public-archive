import React, { useState, useEffect } from "react";
import api from 'lib/api';

const AddPlayer = ({ onSubmit }) => {
    const [homePlayerId, setHomePlayerId] = useState("");
    const [visitorPlayerId, setVisitorPlayerId] = useState("");
    const [homeResult, setHomeResult] = useState("");
    const [visitorResult, setVisitorResult] = useState("");
    const [finished, setFinished] = useState(false);
    const [players, setPlayers] = useState([]);
    useEffect(() => {
        api.playersApi.playersControllerGetAll((error, data, { body }) => {
            if (error) {
                console.error(error);
            } else {
                setPlayers(body);
                setHomePlayerId(body?.[0].id)
                setVisitorPlayerId(body?.[1].id)
            }
        });
    }, [])
    const handleSubmit = async () => {
        const createGameDto = new api.TableFootball.CreateGameDto(Number(homePlayerId), Number(visitorPlayerId), Number(homeResult), Number(visitorResult), finished);
        onSubmit(createGameDto);
    }
    if (players.length < 2) {
        return <h3>No players found or enough players, cant add game</h3>
    }
    return <div>
        <h3>Add new Game</h3>

        <label >Home Player</label>
        <select name="teams" id="teams" value={homePlayerId} required onChange={({ target }) => { setHomePlayerId(target.value) }} >
            {
                players.map((player) => <option key={player.id} value={player.id} >{player.name}</option>)
            }
        </select>


        <label >Visitor Player</label>
        <select name="teams" id="teams" value={visitorPlayerId} required onChange={({ target }) => { setVisitorPlayerId(target.value) }} >
            {
                players.map((player) => <option key={player.id} value={player.id} >{player.name}</option>)
            }
        </select>


        <label >Home Results</label>
        <input onChange={({ target }) => { setHomeResult(target.value) }} required />


        <label >Visitor Results</label>
        <input onChange={({ target }) => { setVisitorResult(target.value) }} required />


        <label >Finished</label>
        <input type="checkbox" onChange={({ target }) => { setFinished(target.checked) }} />
        <br />

        <button onClick={handleSubmit}>Add</button>
    </div>
}

export default AddPlayer;