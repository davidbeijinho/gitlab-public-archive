import React from "react";

const ListGames = ({ games }) => {
  if (games.length === 0) {
    return <h3>No Games found</h3>
  }
  return <div>
    <h3>Games list</h3>
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Home Player</th>
          <th>Visitor Player</th>
          <th>Home Result</th>
          <th>Visitor Result</th>
          <th>Finished</th>
        </tr>
      </thead>
      <tbody>
        {games.map(({ id, homePlayer: { name: homePlayerName }, visitorPlayer: { name: visitorPlayerName }, homeResult, visitorResult, finished }) => <tr key={id}>
          <td>{id}</td>
          <td>{homePlayerName}</td>
          <td>{visitorPlayerName}</td>
          <td>{homeResult}</td>
          <td>{visitorResult}</td>
          <td>{finished ? "Finished" : "In progress"}</td>
        </tr>)}
      </tbody>
    </table>
  </div >
}

export default ListGames;