import React, { useEffect, useState } from "react"
import AddGame from 'pages/games/AddGame';
import ListGames from 'pages/games/ListGames';
import api from 'lib/api';

const Games = () => {
    const [addMode, setAddMode] = useState(false);
    const [games, setGames] = useState([]);

    const loadGames = () => {
        api.gamesApi.gamesControllerGetAll((error, data, { body }) => {
            if (error) {
                console.error(error);
            } else {
                setGames(body);
            }
        });
    }
    useEffect(() => {
        loadGames();
    }, [])
    const onAdd = (dto) => {
        api.gamesApi.gamesControllerCreate(dto, (error) => {
            if (error) {
                console.error(error);
            } else {
                setAddMode(false);
                loadGames();
            }
        });
    }
    return <>
        <h1>Games page</h1>
        <button onClick={() => setAddMode(!addMode)}>{addMode ? "Show games list" : "Add new game"}</button>
        { addMode ? <AddGame onSubmit={onAdd} /> : <ListGames games={games} />}
    </>
}

export default Games