import * as TableFootball from '../client/src';

const api = {
    TableFootball,
    teamsApi: new TableFootball.TeamsApi(),
    playersApi: new TableFootball.PlayersApi(),
    gamesApi: new TableFootball.GamesApi(),
}

export default api;