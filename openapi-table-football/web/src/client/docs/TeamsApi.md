# table_football_client.TeamsApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**teamsControllerCreate**](TeamsApi.md#teamsControllerCreate) | **POST** /teams | Create team
[**teamsControllerFindOne**](TeamsApi.md#teamsControllerFindOne) | **GET** /teams/{id} | Get team by {id}
[**teamsControllerGetAll**](TeamsApi.md#teamsControllerGetAll) | **GET** /teams | Get all teams



## teamsControllerCreate

> Team teamsControllerCreate(createTeamDto)

Create team

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.TeamsApi();
let createTeamDto = new table_football_client.CreateTeamDto(); // CreateTeamDto | 
apiInstance.teamsControllerCreate(createTeamDto, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createTeamDto** | [**CreateTeamDto**](CreateTeamDto.md)|  | 

### Return type

[**Team**](Team.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## teamsControllerFindOne

> Team teamsControllerFindOne(id)

Get team by {id}

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.TeamsApi();
let id = "id_example"; // String | 
apiInstance.teamsControllerFindOne(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Team**](Team.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## teamsControllerGetAll

> teamsControllerGetAll()

Get all teams

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.TeamsApi();
apiInstance.teamsControllerGetAll((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

