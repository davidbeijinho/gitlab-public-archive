# table_football_client.DefaultApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**appControllerGetHello**](DefaultApi.md#appControllerGetHello) | **GET** / | 



## appControllerGetHello

> String appControllerGetHello()



### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.DefaultApi();
apiInstance.appControllerGetHello((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

