# table_football_client.Game

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**homePlayer** | [**Player**](Player.md) | The Player Id of the home Player | 
**visitorPlayer** | [**Player**](Player.md) | The Player Id of the visitor Player | 
**homeResult** | **Number** | The number of goals for the home Player | 
**visitorResult** | **Number** | The number of goals for the visitor Player | 
**finished** | **Boolean** | flag that indicates if the game as finished | 
**id** | **Number** |  | 


