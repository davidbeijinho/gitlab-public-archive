# table_football_client.Team

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the Team | 
**id** | **Number** |  | 


