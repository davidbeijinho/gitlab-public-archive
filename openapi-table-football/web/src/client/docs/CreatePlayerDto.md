# table_football_client.CreatePlayerDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The Name of the Player | 
**teamId** | **Number** | The Id of the Team that the player belongs | 


