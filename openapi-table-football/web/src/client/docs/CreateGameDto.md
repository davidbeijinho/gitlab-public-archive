# table_football_client.CreateGameDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**homePlayerId** | **Number** | The Player Id of the home Player | 
**visitorPlayerId** | **Number** |  | 
**homeResult** | **Number** | The number of goals of the home Player | 
**visitorResult** | **Number** | The number of goals of the visitor Player | 
**finished** | **Boolean** | Flag that indicates if the game was finished | 


