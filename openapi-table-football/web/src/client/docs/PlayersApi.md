# table_football_client.PlayersApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playersControllerCreate**](PlayersApi.md#playersControllerCreate) | **POST** /players | Create team
[**playersControllerFindOne**](PlayersApi.md#playersControllerFindOne) | **GET** /players/{id} | Get player by {id}
[**playersControllerGetAll**](PlayersApi.md#playersControllerGetAll) | **GET** /players | Get all players



## playersControllerCreate

> Player playersControllerCreate(createPlayerDto)

Create team

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.PlayersApi();
let createPlayerDto = new table_football_client.CreatePlayerDto(); // CreatePlayerDto | 
apiInstance.playersControllerCreate(createPlayerDto, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createPlayerDto** | [**CreatePlayerDto**](CreatePlayerDto.md)|  | 

### Return type

[**Player**](Player.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## playersControllerFindOne

> Player playersControllerFindOne(id)

Get player by {id}

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.PlayersApi();
let id = "id_example"; // String | 
apiInstance.playersControllerFindOne(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Player**](Player.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## playersControllerGetAll

> playersControllerGetAll()

Get all players

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.PlayersApi();
apiInstance.playersControllerGetAll((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

