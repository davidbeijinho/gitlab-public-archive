# table_football_client.GamesApi

All URIs are relative to *http://localhost:9000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**gamesControllerCreate**](GamesApi.md#gamesControllerCreate) | **POST** /games | Create game
[**gamesControllerFindOne**](GamesApi.md#gamesControllerFindOne) | **GET** /games/{id} | Get game by {id}
[**gamesControllerGetAll**](GamesApi.md#gamesControllerGetAll) | **GET** /games | Get all games



## gamesControllerCreate

> Game gamesControllerCreate(createGameDto)

Create game

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.GamesApi();
let createGameDto = new table_football_client.CreateGameDto(); // CreateGameDto | 
apiInstance.gamesControllerCreate(createGameDto, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createGameDto** | [**CreateGameDto**](CreateGameDto.md)|  | 

### Return type

[**Game**](Game.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## gamesControllerFindOne

> Game gamesControllerFindOne(id)

Get game by {id}

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.GamesApi();
let id = "id_example"; // String | 
apiInstance.gamesControllerFindOne(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Game**](Game.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## gamesControllerGetAll

> gamesControllerGetAll()

Get all games

### Example

```javascript
import table_football_client from 'table_football_api';

let apiInstance = new table_football_client.GamesApi();
apiInstance.gamesControllerGetAll((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

