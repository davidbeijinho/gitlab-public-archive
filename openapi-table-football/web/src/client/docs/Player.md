# table_football_client.Player

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the Player | 
**team** | [**Team**](Team.md) | The Id of the Team that the player belongs | 
**id** | **Number** |  | 


